Copy the files to VBA RRs `lua` folder, load a game and start one of the scripts at Tools -> Lua scripting -> New Lua Script Window...

Whenever text is loaded you can press Select and it will be copied directly to your clipboard. This is very useful if you want to document stuff without having to type everything. For example I created these scripts since I wanted to extract all supports of EU roms but EU roms are not very well documented (Mostly because it wouldn't make sense to use them for hacking due to their limited free space)

I imagine that it could be useful for other international communities that want to document Fire Emblem Supports and story dialogue in their language as well. Feel free to use the scripts as you want.
